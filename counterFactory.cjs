function counterFactory(counter = 0){
    if(!Number.isNaN(counter)){
        return {
            increment() {
                return ++counter;
            },
            decrement() {
                return --counter;
            },
        };
    }
    else{
        return 0;
    }
};
module.exports = counterFactory;

