const cacheFunction = require('../cacheFunction.cjs')
function callBackFunction(...argumentsPassed) {
    let returnStatement = argumentsPassed.toString();
    return returnStatement;
}

let innerCacheFunction = cacheFunction(callBackFunction);
let name = "shubham", surname = 'wadkar', address = 'bangalore';
console.log(innerCacheFunction(name, surname, address));
console.log(innerCacheFunction(address));
console.log(innerCacheFunction(address, name));
console.log(innerCacheFunction(name, surname, address));
