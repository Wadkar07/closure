function cacheFunction(callBackFunction) {
    const cache = {};
    if (typeof callBackFunction === 'function') {
        return function innerFunction(...argumentsPassed) {
            const argumentsPassedString = JSON.stringify(argumentsPassed);
            if (cache[argumentsPassedString]!==undefined) {
                return cache[argumentsPassedString];
            }
            else {
                const result = callBackFunction(...argumentsPassed);
                cache[argumentsPassedString] = result;
                return result;
            }
        }
    }
    else {
        throw new Error("Type mismatch");
    }
};

module.exports = cacheFunction;
