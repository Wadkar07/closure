function limitFunctionCallCount(callBackFunction, numberOfCalls) {
    let functionCallLimit = 0;
    if (typeof callBackFunction === 'function' && typeof numberOfCalls === 'number') {
        return function innerFunction(...argumentsPassed) {
            if (functionCallLimit === numberOfCalls) {
                return null;
            }
            else {
                functionCallLimit += 1;
                return callBackFunction(...argumentsPassed);
            }
        }
    }
    else {
        throw new Error("type mismatch");
    }
};
module.exports = limitFunctionCallCount;
